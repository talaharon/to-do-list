import { ArgvParser } from "./modules/argv-parser.js";
import Controller from "./modules/controller.js";




const init = async () => {
    let commands = await ArgvParser.parse(process.argv);
    const controller = new Controller();
    commands = controller.validateCommands(commands);
    await controller.start(commands);
};

init();