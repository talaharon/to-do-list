import { ICommand } from "./interfaces";

export class ArgvParser {
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    private constructor() {}

    public static parse = (argv: string[]): ICommand[] => {
        const commands: ICommand[] = [];
        let lastIndex = -1;
        for (const input of argv.slice(2)) {
            if (input[0] === "-" && lastIndex !== -1) {
                const argRegex = new RegExp("-+([a-zA-Z]+)=(.*)");
                const commandArgs: RegExpMatchArray | null =
                    input.match(argRegex);
                if (commandArgs !== null && commandArgs.length >= 3) {
                    commands[lastIndex].arguments[commandArgs[1]] =
                        commandArgs[2];
                }
            } else {
                const command: ICommand = {
                    title: input,
                    arguments: {},
                };
                commands.push(command);
                lastIndex++;
            }
        }
        return commands;
    };
}
