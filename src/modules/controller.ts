import log from "@ajar/marker";
import { ICommand, IHelpObj } from "./interfaces.js";
import ToDoList from "./todo-list.js";
import * as utils from "./utils.js";

export default class Controller {
    private helpObj: IHelpObj = {
        create: {
            description: "Create a new task",
            parameters: "Title: Task's title",
            examples:
                "node ./dist/app.js create (or c) --title='My First Note'",
            shortcut: "c",
        },
        update: {
            description:
                "Change task status from active to inactive and vice-versa",
            parameters: "Id: the id of the task you would like to update",
            examples: "node ./dist/app.js update (or u) --id=[ID]",
            shortcut: "u",
        },
        remove: {
            description: "Remove task by ID",
            parameters: "Id: the id of the task you would like to remove",
            examples: "node ./dist/app.js remove (or rm) --id=[ID]",
            shortcut: "rm",
        },
        read: {
            description: "Get all tasks and display them to the console",
            parameters:
                "Filter: Filter displayed tasks by: 'All', 'Completed' or 'Open'",
            examples: "node ./dist/app.js read (or r) --filter:'Completed'",
            shortcut: "r",
        },
        removecompleted: {
            description: "Remove all completed tasks",
            parameters: "None",
            examples: "node ./dist/app.js removecompleted (or rc)",
            shortcut: "rc",
        },
    };

    start = async (commands: ICommand[]): Promise<void> => {
        await ToDoList.init();
        for (const cmd of commands) {
            const cmdName = cmd.title.toLowerCase();
            if (cmdName === "create" || cmdName === "c") {
                if (cmd.arguments.title === undefined) {
                    cmd.arguments.title = utils.askInput("Title");
                }
                await ToDoList.create(cmd.arguments.title);
            } else if (cmdName === "read" || cmdName === "r") {
                if (cmd.arguments.filter === undefined) {
                    cmd.arguments.filter = "";
                }
                await ToDoList.read(cmd.arguments.filter);
            } else if (cmdName === "update" || cmdName === "u") {
                if (cmd.arguments.id === undefined) {
                    cmd.arguments.id = utils.askInput("ID");
                }
                await ToDoList.update(cmd.arguments.id);
            } else if (cmdName === "remove" || cmdName === "rm") {
                if (cmd.arguments.id === undefined) {
                    cmd.arguments.id = utils.askInput("ID");
                }
                await ToDoList.remove(cmd.arguments.id);
            } else if (cmdName === "removecompleted" || cmdName === "rc") {
                await ToDoList.removeAllCompleted();
            } else {
                this.printHelper();
            }
        }
    };

    public validateCommands = (inputCommands: ICommand[]): ICommand[] => {
        const allCommands = [
            ...Object.keys(this.helpObj),
            ...Object.values(this.helpObj).map((val) => val.shortcut),
        ];
        for (const command of inputCommands) {
            if (!allCommands.includes(command.title)) {
                return [
                    {
                        title: "help",
                        arguments: {},
                    },
                ];
            }
        }
        return inputCommands;
    };

    private printHelper = (): void => {
        log.magenta(
            "/********************************** Help **********************************/"
        );
        log.cyan("Commands:");
        for (const key in this.helpObj) {
            log.yellow(key);
            log.green("         Description: ", this.helpObj[key].description);
            log.green("         Shortcut:   ",  this.helpObj[key].shortcut);
            log.green("         Parameters:  ", this.helpObj[key].parameters);
            log.green("         Examples:    ", this.helpObj[key].examples);
        };
    };
}
